//
//  BpViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpViewController.h"

@interface BpViewController ()

@end 

@implementation BpViewController
@synthesize imgslpashView;
@synthesize gBpMainViewController,navController;

- (void)updateTimer{ //Happens every time updateTimer is called. Should occur every second.
    
    counterA -= 1;
    _CountDownTime = [NSString stringWithFormat:@"%i", counterA];
    
    if (counterA < 0) // Once timer goes below 0, reset all variables.
    {
        [timerA invalidate];
        startA = TRUE;
        //counterA = 1;
        self.imgslpashView.hidden=YES;
        
        //[self initializeAfterViewLoad];
    }
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    counterA = 1;
    startA = TRUE;
    
    if(startA == TRUE) //Check that another instance is not already running.
    {
        _CountDownTime = @"1";
        startA = FALSE;
        timerA = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
        
        
        
    }   

    

    [self performSelector:@selector(BpMainPage) withObject:nil afterDelay:2];
    //[self initializeAfterViewLoad];
}

-(void)initializeAfterViewLoad
{
    //[self performSelector:@selector(BpMainPage) withObject:nil afterDelay:2];
    
    BpMainViewController *gtBpMainViewController=[[BpMainViewController alloc] initWithNibName:@"BpMainViewController" bundle:nil];
    
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gtBpMainViewController];
    //tnavController.view.frame=CGRectMake(0, 0, 320, 416);
    gtBpMainViewController.title=@"Main";
    UIImage *imagetopbar = [UIImage imageNamed:@"bar_top.png"];
    [tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    //[self.view addSubview:tnavController.view];
    
    [self.navigationController pushViewController:gBpMainViewController animated:YES];
}

-(void)BpMainPage
{
    for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    BpSplashViewController *gBpSplashViewController=[[BpSplashViewController alloc] initWithNibName:@"BpSplashViewController"  bundle:nil];
    //for (UIView* uiview in self.view.subviews) {
    //    [uiview removeFromSuperview];
    //}
    gBpSplashViewController.view.frame=CGRectMake(0, 0, 320, self.view.frame.size.height);
    [self.view addSubview:gBpSplashViewController.view];
}

- (void)viewDidUnload
{
    [self setImgslpashView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return interfaceOrientation == UIInterfaceOrientationPortrait;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)dealloc
{
    [navController release];
    [imgslpashView release];
    [super dealloc];
}

@end
