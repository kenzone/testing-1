//
//  BpReferenceNo.h
//  BP Healthcare
//
//  Created by desmond on 13-1-18.
//
//

#import <Foundation/Foundation.h>

@interface BpReferenceNo : NSObject

@property (copy, nonatomic) NSString *ReferenceNumber;

@end
