//
//  BpBirthday.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BpBirthday : NSObject
{
    
}
/*<resultCount>string</resultCount>
 <birthday_imgID>string</birthday_imgID>
 <birthday_imgPath>string</birthday_imgPath>*/

@property (nonatomic,retain) NSString *resultCount;
@property (nonatomic,retain) NSString *birthday_imgID;
@property (nonatomic,retain) NSString *birthday_imgPath;

@end
