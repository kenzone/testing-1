//
//  BpYourPointViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpYourPointViewController.h"

@interface BpYourPointViewController ()

@end

@implementation BpYourPointViewController
@synthesize yourpointtable;
@synthesize textfieldName;
@synthesize lblRewardPoint;
@synthesize lblRebatePoint, loadingView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    BpYourPointViewController *gBpYourPointViewController=[[BpYourPointViewController alloc] initWithNibName:@"BpYourPointViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpYourPointViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpYourPointViewController release];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        
        NSLog(@"code for 4-inch screen");
        [self fview];
        
    } else {
        // code for 3.5-inch screen
        NSLog(@"code for 3.5-inch screen");

        
    }
    
    yourpointtable.layer.cornerRadius=10;
    yourpointtable.layer.borderColor = [UIColor colorWithRed:(170/255.0) green:(170/255.0) blue:(170/255.0) alpha:1.0].CGColor;
    //[UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
    yourpointtable.layer.borderWidth = 1;
    
    /*tableView.layer.cornerRadius = 10.0f;
     tableView.layer.borderColor = [UIColor grayColor].CGColor;
     tableView.layer.borderWidth = 1;*/
    
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSString *icdelegate=[[NSString alloc]initWithFormat:@"%@",appDelegate.loginStatus.ICNO];
    
    if ([icdelegate isEqualToString:@"(null)"]) {
        [self someMethod];
    }else {
        [self getMyPoint:appDelegate.loginStatus.ICNO];
    }
    
    [icdelegate release];
}

-(void)fview
{
    yourpointtable.frame=CGRectMake(20, 163, 280, 89);
}

-(void)getMyPoint:(NSString*)icNum
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpMyPointRequest *loginset= [[BpMyPointRequest alloc] initWithisicno:icNum];
    
    [networkHandler setDelegate:self];
    [networkHandler request:loginset];
    [loginset release];
    [networkHandler release];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
}

- (void)viewDidUnload
{
    [self setYourpointtable:nil];
    [self setLblRewardPoint:nil];
    [self setLblRebatePoint:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) someMethod
{
    
    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please provide your IC/Passport" message:@""
                                                   //delegate:self cancelButtonTitle:@"Cancel"  otherButtonTitles:@"Submit", nil];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please proceed login from main page." message:@""
                                                   delegate:self cancelButtonTitle:nil  otherButtonTitles:@"Ok", nil];
        
        //[alert addTextFieldWithValue:@"" label:@"IC/Passport"];
        //[alert addTextFieldWithValue:@"" label:@"Password"];
        
        // Username
        /*textfieldName = [alert textFieldAtIndex:0];
        
        textfieldName.clearButtonMode = UITextFieldViewModeWhileEditing;
        textfieldName.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        
        textfieldName.keyboardType = UIKeyboardTypeAlphabet;
        textfieldName.keyboardAppearance = UIKeyboardAppearanceAlert;
        textfieldName.autocorrectionType = UITextAutocorrectionTypeNo;*/
        
        // Password
        /*textfieldPassword = [alert textFieldAtIndex:1];
         textfieldPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
         textfieldPassword.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
         textfieldPassword.keyboardAppearance = UIKeyboardAppearanceAlert;
         textfieldPassword.autocorrectionType = UITextAutocorrectionTypeNo;
         textfieldPassword.secureTextEntry = YES;*/
        
        [alert show];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Clicked the Submit button
    if (buttonIndex != [alertView cancelButtonIndex])
    {
        /*if(![self NetworkStatus])
         {
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
         [alert show];
         [alert release];
         }
         else
         {*/
        NSLog(@"Name: %@", textfieldName.text);
        //NSLog(@"Password : %@", textfieldPassword.text);
        //[leftloginButton setImage:[UIImage imageNamed:@"btn_logout.png"] forState:UIControlStateNormal]; 
        
        //loginStatus=YES;
        
        /*if ([textfieldName.text length] !=0) {
            NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
            BpMyPointRequest *loginset= [[BpMyPointRequest alloc] initWithisicno:textfieldName.text];
            
            [networkHandler setDelegate:self];
            [networkHandler request:loginset];
            [loginset release];
            [networkHandler release];
            
            UIView *selfView=self.view;
            LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
            self.loadingView=temploadingView;
            
        }else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please provide your IC/Passport." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert autorelease];
            [alert show];
            
            [self.navigationController popViewControllerAnimated:YES];
        }*/
        [self.navigationController popViewControllerAnimated:YES];
        /*
         BOOL isLoadingDisplayed=NO;
         for(UIView *subView in self.view.subviews)
         {
         if ([subView class]==[LoadingView class]) {
         isLoadingDisplayed=YES;
         }
         }
         if(!isLoadingDisplayed)
         {
         UIView *selfView=self.view;
         LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
         self.loadingView=temploadingView;
         }*/
        //}
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
} 

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpMyPointResponse class]])
    {
        
        //NSString *strMessage=[NSString stringWithFormat:@"%@",((BpCheckUpResponse*)responseMessage).CheckUpArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpMyPointResponse*)responseMessage).MyPointArr;
        
        for (BpMyPoint *item in msgArr) {
            
            NSLog(@"birthitem.resultCount--->%@",item.resultCount);
            NSLog(@"birthitem.PointEarn--->%@",item.PointEarn);
            NSLog(@"birthitem.PointRebate--->%@",item.PointRebate);

            lblRewardPoint.text=item.PointEarn;
            lblRebatePoint.text=item.PointRebate;
        }
        [self.loadingView removeView];
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}

- (void)dealloc 
{
    [loadingView release];
    [textfieldName release];
    [yourpointtable release];
    [lblRewardPoint release];
    [lblRebatePoint release];
    [super dealloc];
}
@end
