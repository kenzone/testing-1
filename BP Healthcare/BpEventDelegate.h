//
//  BpEventDelegate.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

/*#ifndef BP_Healthcare_BpEventDelegate_h
#define BP_Healthcare_BpEventDelegate_h



#endif*/

#import <Foundation/Foundation.h>


@protocol BpEventDelegate <NSObject>
//-(void)editSaleOrderForEditButton;
//-(void)reloadNewOrderTable;
//-(void)editSaleOrderPDFButton:(SalesOrder*)isaleOrder SalesPersonCode:(SalesPerson*)isalesPersonCode;
//-(void)editSaleOrderPDFButton:(SalesOrder *)isaleOrder SalesPersonCode:(NSString *)isalesPersonCode;
-(void)addEventTapped:(BpEvent*)iBpEvent;
-(void)enlargeEventimgTapped;
-(void)imgurlTapped:(NSString*)imagename;
@end