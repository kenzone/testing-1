//
//  BpSelectBranchViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "NetworkHandler.h"
#import "BpOutlet.h"
#import "BpOutletRequest.h"
#import "BpOutletResponse.h"
#import "LoadingView.h"

@class BpNewAppointmentViewController;

@interface BpSelectBranchViewController : UIViewController<NetworkHandlerDelegate>{
    
}
-(void)getBranch;
@property (nonatomic, retain) BpNewAppointmentViewController *gBpNewAppointmentViewController;
@property (retain, nonatomic) IBOutlet UITableView *tblBranchList;
@property (retain, nonatomic) NSArray * Branch;
@property (retain, nonatomic) NSString *selectedState;
@property (nonatomic, retain) LoadingView *loadingView;

@end
