//
//  BpPromotions.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpPromotions.h"

@implementation BpPromotions
@synthesize resultCount, promotionID, promotionSort, promotionDescp, promotionTitle, promotionStatus, promotionImgPath, promotionDateTime;

-(void)dealloc
{
    [promotionDateTime release];
    [promotionDescp release];
    [promotionID release];
    [promotionImgPath release];
    [promotionSort release];
    [promotionStatus release];
    [promotionTitle release];
    [resultCount release];
    [super dealloc];
}
@end
