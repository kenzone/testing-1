//
//  BpPromotionItem.m
//  BP Healthcare
//
//  Created by desmond on 13-1-7.
//
//

#import "BpPromotionItem.h"

@implementation BpPromotionItem

@synthesize Name;
@synthesize CreateDate;
@synthesize EditDate;
@synthesize Descp;
@synthesize DiscountPrice;
@synthesize NormalPrice;
@synthesize PicURl;

-(void)dealloc
{
    [Name release];
    [CreateDate release];
    [EditDate release];
    [Descp release];
    [DiscountPrice release];
    [NormalPrice release];
    [PicURl release];
    [super dealloc];
}

@end
