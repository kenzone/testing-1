//
//  BpAppointmentLsViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpAppointmentLsCell.h"
#import "BpAppointmentList.h"
#import "BpAppDelegate.h"

@class BpAppointmentViewController;

@interface BpAppointmentLsViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource>{
    BpAppointmentLsCell *cell;
}

@property (nonatomic, retain) BpAppointmentViewController* gBpAppointmentViewController;
@property (nonatomic, retain) NSArray *AppointmentLsArr;
-(void)initializeTableData;

@end
