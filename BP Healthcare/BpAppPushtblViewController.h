//
//  BpAppPushtblViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpAppPushCell.h"
#import "BpAppointmentList.h"
#import "BpAppDelegate.h"

@class BpAppPushViewController;

@interface BpAppPushtblViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource>{
    BpAppPushCell *cell;
}

@property (nonatomic, retain) BpAppPushViewController* gBpAppointmentViewController;
@property (nonatomic, retain) NSArray *AppointmentLsArr;
-(void)initializeTableData;

@end
