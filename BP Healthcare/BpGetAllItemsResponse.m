//
//  BpGetAllItemsResponse.m
//  BP Healthcare
//
//  Created by desmond on 13-1-7.
//
//

#import "BpGetAllItemsResponse.h"

@implementation BpGetAllItemsResponse
@synthesize PromotionsArr;

-(void)dealloc
{
    [PromotionsArr release];
    [super dealloc];
}

@end
