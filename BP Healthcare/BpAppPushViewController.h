//
//  BpAppPushViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "BpAppointmentList.h"
#import "BpAppointmentLsRequest.h"
#import "BpAppointmentLsResponse.h"
#import "NetworkHandler.h"
#import "Reachability.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "BpAppPushtblViewController.h"
#import "BpNewAppointmentViewController.h"
#import "LoadingView.h"
#import "BpAppDelegate.h"
#import "BpMainViewController.h"

@interface BpAppPushViewController : UIViewController<NetworkHandlerDelegate>
{
    
}
-(void)NewAppointmentTapped;
-(void)getAppointmentList;
@property (nonatomic, retain) NSArray *AppointmentLsArr;
@property (retain, nonatomic) IBOutlet UITableView *tblAppointmentList;
@property (retain , nonatomic) BpAppPushtblViewController* gBpAppointmentLsViewController;

@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) EKEventViewController *detailViewController;
@property (nonatomic, retain) EKEventStore *eventStore;
@property (nonatomic, retain) EKCalendar *defaultCalendar;
@property (nonatomic, retain) NSMutableArray *eventsList;
-(IBAction)MainViewTappedd:(id)sender;
@end
