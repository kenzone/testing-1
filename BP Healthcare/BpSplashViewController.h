//
//  BpSplashViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpMainViewController.h"
@interface BpSplashViewController : UIViewController
{
    
}

@property (nonatomic,retain) UINavigationController* navController;
@property (nonatomic,retain) BpMainViewController *gBpMainViewController;

@end
