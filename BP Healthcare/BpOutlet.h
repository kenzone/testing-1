//
//  BpOutlet.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BpOutlet : NSObject
{
    
}

@property (retain, nonatomic) NSString *resultCount;
@property (retain, nonatomic) NSString *RowID;
@property (retain, nonatomic) NSString *BranchName;
@property (retain, nonatomic) NSString *Address_1;
@property (retain, nonatomic) NSString *Address_2;

@property (retain, nonatomic) NSString *Address_3;
@property (retain, nonatomic) NSString *Tel;
@property (retain, nonatomic) NSString *Fax;
@property (retain, nonatomic) NSString *LocationID;
@property (retain, nonatomic) NSString *BranchImg;

@property (retain, nonatomic) NSString *LocationImg;
@property (retain, nonatomic) NSString *BusinessHourImg;
@property (retain, nonatomic) NSString *TypeDC;
@property (retain, nonatomic) NSString *TypeLAB;
@property (retain, nonatomic) NSString *TypeHEARING;

@property (retain, nonatomic) NSString *TypeDISPENSARY;
@property (retain, nonatomic) NSString *MapURL;
@property (retain, nonatomic) NSString *Latitude;
@property (retain, nonatomic) NSString *Longitude;
@property (retain, nonatomic) NSString *Visibility;

@property (retain, nonatomic) NSString *currentLatitude;
@property (retain, nonatomic) NSString *currentLongitude;


@end




