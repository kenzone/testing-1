//
//  BpIpay88ViewController.h
//  BP Healthcare
//
//  Created by desmond on 13-1-15.
//
//

#import <UIKit/UIKit.h>
#import "Ipay.h"

@protocol ResultDelegate <NSObject>
@required
-(void)paymentResult:(NSDictionary *)result;
@end

@interface BpIpay88ViewController : UIViewController<PaymentResultDelegate>{
   Ipay *paymentsdk;
}

@property (retain, nonatomic) id<ResultDelegate> resultDelegate;
@property (retain, nonatomic) Ipay *paymentsdk;

@property (retain, nonatomic) IBOutlet UIView *resultView;
@property (retain, nonatomic) UIView *paymentView;
@property (retain, nonatomic) IBOutlet UIImageView *resultImage;

//payment data
@property (retain, nonatomic) NSString *amount;
@property (retain, nonatomic) NSString *prodDesc;
@property (retain, nonatomic) NSString *userName;
@property (retain, nonatomic) NSString *userEmail;
@property (retain, nonatomic) NSString *userContact;
@property (retain, nonatomic) NSString *remark;
@property (retain, nonatomic) NSString *referenceNumber;

@end
