//
//  BpBirthdayViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpBirthdayViewController.h"

@interface BpBirthdayViewController ()

@end

@implementation BpBirthdayViewController
@synthesize imgpicture;
@synthesize imgPicbirthday;
@synthesize loadingView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpBirthdayViewController *gBpMainViewController=[[BpBirthdayViewController alloc] initWithNibName:@"BpBirthdayViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpMainViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpMainViewController release];
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
    [self getBirthday];
}

- (void)viewDidUnload
{
    [self setImgPicbirthday:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getBirthday
{
    
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpBirthdayRequest *loginset= [[BpBirthdayRequest alloc] init];
     
    [networkHandler setDelegate:self];
    [networkHandler request:loginset];
    [loginset release];
    [networkHandler release];
}
-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpBirthdayResponse class]])
    {
        
        NSString *strMessage=[NSString stringWithFormat:@"%@",((BpBirthdayResponse*)responseMessage).birthdayArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpBirthdayResponse*)responseMessage).birthdayArr;
        
        
        
        NSLog(@"strMessage--->%@",strMessage);
        
        
        for (BpBirthday *birthitem in msgArr) {
            //birthitem.resultCount;
            NSLog(@"birthitem.resultCount--->%@",birthitem.resultCount);
            NSLog(@"birthitem.birthday_imgID--->%@",birthitem.birthday_imgID);
            NSLog(@"birthitem.birthday_imgPath--->%@",birthitem.birthday_imgPath);
            
            
            NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/birthday/%@",birthitem.birthday_imgPath];
            
            NSString *descriptiono=[imagename stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            imgPicbirthday.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:descriptiono]]];
            //NSLog(@"imagename--->%@",imagename);
            //NSLog(@"imgPicbirthday.image--->%@",imgPicbirthday.image);
            [self.loadingView removeView];
        }
        
        
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}

-(void)dealloc
{
    [loadingView release];
    [imgpicture release];
    [imgPicbirthday release];
    [super dealloc];
    
}
@end
