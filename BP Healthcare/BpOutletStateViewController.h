//
//  BpOutletStateViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "BpOurOutletViewController.h"

@interface BpOutletStateViewController : UIViewController{
    
}

@property (retain, nonatomic) IBOutlet UITableView *tblOutletState;
@property (retain, nonatomic) NSArray * State;
@property (retain, nonatomic) NSString *SelectedState;

@end
