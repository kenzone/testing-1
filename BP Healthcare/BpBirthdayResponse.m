//
//  BpBirthdayResponse.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpBirthdayResponse.h"

@implementation BpBirthdayResponse
@synthesize birthdayArr;

-(void)dealloc
{
    [birthdayArr release];
    [super dealloc];
}

@end
