//
//  BpLocation.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
@interface BpLocation : NSObject{
	double latitude;
	double longitude;
	double altitude;
    
}
@property (readwrite,nonatomic,assign) double latitude;
@property (readwrite,nonatomic,assign) double longitude;
@property (readwrite,nonatomic,assign) double altitude;

-(id)initWithLatitude:(double)ilatitude longitude:(double)ilongitude;
- (double)distanceFromLocation:(const BpLocation *)location;
@end
