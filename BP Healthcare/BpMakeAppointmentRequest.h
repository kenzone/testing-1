//
//  BpMakeAppointmentRequest.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"
#import "BpAppointment.h"

@interface BpMakeAppointmentRequest : XMLRequest{
    
}

-(id)initWithsAppointment:(BpAppointment *)iAppointment;

@property (retain, nonatomic) NSString *resultCount;
@property (retain, nonatomic) NSString *appointmentID;
@property (retain, nonatomic) NSString *appointmentServiceRequest;
@property (retain, nonatomic) NSString *appointmentSort;
@property (retain, nonatomic) NSString *appointmentStatus;

@property (retain, nonatomic) NSString *appointmentNoOfPerson;
@property (retain, nonatomic) NSString *appointmentCreateDate;
@property (retain, nonatomic) NSString *appointmentBookDateTime;
@property (retain, nonatomic) NSString *appointmentBranch;
@property (retain, nonatomic) NSString *userID;

@property (retain, nonatomic) NSString *tokenID;
@property (retain, nonatomic) NSString *adminRemark;
@property (retain, nonatomic) NSString *customerName;
@property (retain, nonatomic) NSString *customerContact;
@property (retain, nonatomic) NSString *customerEmail;

@property (retain, nonatomic) NSString *customerRemark;
@property (retain, nonatomic) NSString *insertResult;

@property (retain, nonatomic) BpAppointment *insertAppointment;

@end
