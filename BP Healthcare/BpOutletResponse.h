//
//  BpOutletResponse.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"

@interface BpOutletResponse : XMLResponse{
    
    
}

@property (retain, nonatomic) NSArray *OutletArr;

@end
