//
//  BpPaymentResult.h
//  BP Healthcare
//
//  Created by desmond on 13-1-23.
//
//

#import <Foundation/Foundation.h>

@interface BpPaymentResult : NSObject

@property (retain, nonatomic) NSString *paymentstatus;
@property (retain, nonatomic) NSString *referenceNo;
@property (retain, nonatomic) NSString *remark;
@property (retain, nonatomic) NSString *amount;
@property (retain, nonatomic) NSString *transactionid;

@end
