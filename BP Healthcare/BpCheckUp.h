//
//  BpCheckUp.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BpCheckUp : NSObject{
    
}

@property (nonatomic,retain) NSString *resultCount;
@property (nonatomic,retain) NSString *checkupID;
@property (nonatomic,retain) NSString *checkupTitle;
@property (nonatomic,retain) NSString *checkupDescp;
@property (nonatomic,retain) NSString *checkupSort;
@property (nonatomic,retain) NSString *checkupStatus;
@property (nonatomic,retain) NSString *checkupDateTime;
@property (nonatomic,retain) NSString *checkupImgPath;


@end
