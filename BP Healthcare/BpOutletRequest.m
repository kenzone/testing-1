//
//  BpOutletRequest.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpOutletRequest.h"

@implementation BpOutletRequest

@synthesize state;

-(id)initWithsState:(NSString *)istate
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"retrieveOutletInfo";
        self.requestType=WebserviceRequest;
        self.state=istate;
    }
    return self;
}


-(void)dealloc
{
    [state release];
    [super dealloc];
}

-(NSString*) generateHTTPPostMessage
{
    
    NSString *postMsg = [NSString stringWithFormat:@"state=%@",self.state];
    return postMsg;
} 

@end
