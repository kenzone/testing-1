//
//  BpPromotionItemCell.m
//  BP Healthcare
//
//  Created by desmond on 13-1-8.
//
//

#import "BpPromotionItemCell.h"

@implementation BpPromotionItemCell

@synthesize titleLabel;
@synthesize promotionPriceLabel;
@synthesize NPLabel;
@synthesize descriptionLabel;
@synthesize imageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setData:(NSString *)data UILabel:(UILabel *)label{
    label.text = data;
}

-(void)setImageSrc:(NSString *)data UILabel:(UIImageView *)image{
    image.image = [UIImage imageNamed:data];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
