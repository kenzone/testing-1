//
//  BpUpdateOrderRequest.m
//  BP Healthcare
//
//  Created by desmond on 13-1-23.
//
//

#import "BpUpdateOrderRequest.h"

@implementation BpUpdateOrderRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"UpdateOrderByRefID";
        self.requestType=WebserviceRequest;
    }
    return self;
}


-(NSString*) generateHTTPPostMessage
{
    NSString *postMsg = [NSString stringWithFormat:
                         @"referenceNo=%@"
                         "&remark=%@"
                         "&transactionid=%@"
                         "&authcode=%@"
                         "&amount=%@"
                         "&paymentstatus=%@",
                         _referenceNo,_remark,
                         _transactionid,_authcode,
                         _amount,_paymentstatus];
    
    //    NSLog(@"postMsg-----> %@",postMsg);
    return postMsg;
}


@end
