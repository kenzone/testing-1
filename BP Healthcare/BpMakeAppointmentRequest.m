//
//  BpMakeAppointmentRequest.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpMakeAppointmentRequest.h"

@implementation BpMakeAppointmentRequest

@synthesize resultCount, adminRemark, appointmentID, appointmentSort, appointmentBranch, appointmentStatus, appointmentCreateDate, appointmentNoOfPerson, appointmentBookDateTime, appointmentServiceRequest, customerName, customerEmail, customerRemark, customerContact, insertResult, userID, tokenID;
@synthesize insertAppointment;


-(id)initWithsAppointment:(BpAppointment *)iAppointment
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"saveAppointment";
        self.requestType=WebserviceRequest;
        self.insertAppointment=iAppointment;
    }
    return self;
}

-(void)dealloc
{
    [resultCount release];
    [appointmentID release];
    [appointmentServiceRequest release];
    [appointmentSort release];
    [appointmentStatus release];
    
    [appointmentNoOfPerson release];
    [appointmentCreateDate release];
    [appointmentBookDateTime release];
    [appointmentBranch release];
    [userID release];
    
    [tokenID release];
    [adminRemark release];
    [customerName release];
    [customerContact release];
    [customerEmail release];
    
    [customerRemark release];
    [insertResult release];
    
    [insertAppointment release];
    [super dealloc];
}

-(NSString*) generateHTTPPostMessage
{
    //userID=string&serviceReq=string&appDate=string&customerName=string&customerContact=string&customerEmail=string&appBranch=string&noOfPerson=string&custRemark=string&tokenID=string
    
    NSString *postMsg = [NSString stringWithFormat:@"userID=%@&serviceReq=%@&appDate=%@&customerName=%@&customerContact=%@&customerEmail=%@&appBranch=%@&noOfPerson=%@&custRemark=%@&tokenID=%@",self.insertAppointment.userID,self.insertAppointment.appointmentServiceRequest, self.insertAppointment.appointmentCreateDate,self.insertAppointment.customerName, self.insertAppointment.customerContact, self.insertAppointment.customerEmail,self.insertAppointment.appointmentBranch,self.insertAppointment.appointmentNoOfPerson,self.insertAppointment.customerRemark,self.insertAppointment.tokenID];
    
    NSLog(@"postMsg----->%@",postMsg);
    return postMsg;
} 


@end

