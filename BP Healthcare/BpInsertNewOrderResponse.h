//
//  BpInsertNewOrderResponse.h
//  BP Healthcare
//
//  Created by desmond on 13-1-18.
//
//
#import <Foundation/Foundation.h>
#import "XMLResponse.h"

@interface BpInsertNewOrderResponse : XMLResponse

@property (nonatomic, retain) NSArray *ReferenceArr;

@end
