//
//  BploginRequest.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"

@interface BploginRequest : XMLRequest{
    
}

@property (retain, nonatomic) NSString *icno;
-(id)initWithisicno:(NSString *)iicno;


@end
