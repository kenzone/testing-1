//
//  BpAppointmentLsRequest.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"
@interface BpAppointmentLsRequest : XMLRequest{
    
}


@property (retain, nonatomic) NSString *tokenId;
-(id)initWithsTokenId:(NSString *)itokenId;

@end
