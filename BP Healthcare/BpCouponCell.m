//
//  BpCouponCell.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpCouponCell.h"

@implementation BpCouponCell
@synthesize lblBodytext, lblTitle, lineColor, imgsmallflag, BpCouponcell, imgpicture,lblBodyNoPictext;
@synthesize url;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        UIImageView *gimgsmallflag= [[UIImageView alloc]initWithFrame:CGRectMake(30, 10, 20, 21)];        
        //gimgsmallflag.image=[UIImage imageNamed:@"btn_more.png"];
        self.imgsmallflag=gimgsmallflag;
        [gimgsmallflag release];
        [self addSubview:imgsmallflag];
        
        UILabel *glblTitle = [[UILabel alloc] initWithFrame:CGRectMake(45, 10, 455, 21)];
        glblTitle.textAlignment = UITextAlignmentLeft;
        glblTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblTitle.backgroundColor = [UIColor clearColor];
        self.lblTitle = glblTitle;
        [glblTitle release];
        [self addSubview:lblTitle];
        
        UIImageView *gimgpicture= [[UIImageView alloc]initWithFrame:CGRectMake(10, 50, 40, 40)];        
        //gimgsmallflag.image=[UIImage imageNamed:@"btn_more.png"];
        self.imgpicture=gimgpicture;
        [gimgpicture release];
        [self addSubview:imgpicture];
        
        UILabel *glblBodytext = [[UILabel alloc] initWithFrame:CGRectMake(80, 50, 455, 45)];
        glblBodytext.textAlignment = UITextAlignmentLeft;
        glblBodytext.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
        glblBodytext.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblBodytext.backgroundColor = [UIColor clearColor];
        self.lblBodytext = glblBodytext;
        [glblBodytext release];
        [self addSubview:lblBodytext];
        
        UILabel *glblBodyNoPictext = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 455, 45)];
        glblBodyNoPictext.textAlignment = UITextAlignmentLeft;
        glblBodyNoPictext.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
        glblBodyNoPictext.backgroundColor = [UIColor clearColor];
        self.lblBodyNoPictext = glblBodyNoPictext;
        [glblBodyNoPictext release];
        [self addSubview:lblBodyNoPictext];
        
        [self performSelector:@selector(resizeimage) withObject:nil afterDelay:1.0];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)resizeimage
{
    if(self.url != nil)
    {
        float width = self.imgpicture.image.size.width * (float)55 / self.imgpicture.image.size.height;
        if(isnan(width))
            [self performSelector:@selector(resizeimage) withObject:nil afterDelay:1.0];
        else{
            //[self.imgpicture setFrame:CGRectMake(320 - width - 5, 3, width, 55.0)];
            
            
            
            CGSize maxsizetitle = CGSizeMake(220, 9999);
            UIFont *thefonttitle = [UIFont fontWithName:@"Helvetica-Bold" size:17];
            CGSize textsizetitle = [lblTitle.text sizeWithFont:thefonttitle constrainedToSize:maxsizetitle lineBreakMode:UILineBreakModeWordWrap];
            
            self.lblTitle.numberOfLines=20;
            self.lblTitle.frame=CGRectMake(80, 10, 220, textsizetitle.height);
            self.lblTitle.lineBreakMode=UILineBreakModeWordWrap;
            //self.lblTitle.text=classBpHealthTips.healthtipsTitle;
            
            float height = self.imgpicture.image.size.height * (float)65 / self.imgpicture.image.size.width;
            self.imgpicture.frame = CGRectMake(10, textsizetitle.height+23, 65, height);
        }
    }
}

-(void)dealloc
{
    [url release];
    [lblBodyNoPictext release];
    [imgpicture release];
    [lineColor release];
    [BpCouponcell release];
    [imgsmallflag release];
    [lblTitle release];
    [lblBodytext release];
    [super dealloc];
}

@end
