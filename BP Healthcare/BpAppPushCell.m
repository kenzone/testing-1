//
//  BpAppPushCell.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppPushCell.h"

@implementation BpAppPushCell

@synthesize lblDate, lblTitle, lblBodytext, lineColor, imgsmallflag, AppointmentListcell;
@synthesize lblRemark, lblDateText, lblRemarktext, delegate;
@synthesize lblStatus, lblStatustext, btnsaveAppointment;
@synthesize imgsmallcancel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        UIImageView *gimgsmallflag= [[UIImageView alloc]initWithFrame:CGRectMake(8, 10, 35, 32)];        
        //gimgsmallflag.image=[UIImage imageNamed:@"btn_more.png"];
        self.imgsmallflag=gimgsmallflag;
        [gimgsmallflag release];
        [self addSubview:imgsmallflag];
        
        UIImageView *gimgsmallcancel= [[UIImageView alloc]initWithFrame:CGRectMake(8, 10, 35, 32)];        
        //gimgsmallflag.image=[UIImage imageNamed:@"btn_more.png"];
        self.imgsmallcancel=gimgsmallcancel;
        [gimgsmallcancel release];
        [self addSubview:imgsmallcancel];
        
        UILabel *glblTitle = [[UILabel alloc] initWithFrame:CGRectMake(50, 15, 455, 21)];
        glblTitle.textAlignment = UITextAlignmentLeft;
        glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        glblTitle.backgroundColor = [UIColor clearColor];
        self.lblTitle = glblTitle;
        [glblTitle release];
        [self addSubview:lblTitle];
        
        UILabel *glblDate = [[UILabel alloc] initWithFrame:CGRectMake(50, 40, 50, 21)];
        glblDate.text=@"Date  ";
        glblDate.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblDate.textAlignment = UITextAlignmentLeft;
        glblDate.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblDate.backgroundColor = [UIColor clearColor];
        self.lblDate = glblDate;
        [glblDate release];
        [self addSubview:lblDate];
        
        
        UILabel *glblStatus = [[UILabel alloc] initWithFrame:CGRectMake(50, 60, 100, 21)];
        glblStatus.text=@"Status  ";
        glblStatus.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblStatus.textAlignment = UITextAlignmentLeft;
        glblStatus.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblStatus.backgroundColor = [UIColor clearColor];
        self.lblStatus = glblStatus;
        [glblStatus release];
        [self addSubview:lblStatus];
        
        
        UILabel *glblRemark = [[UILabel alloc] initWithFrame:CGRectMake(50, 80, 100, 21)];
        glblRemark.text=@"Remark  ";
        glblRemark.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblRemark.textAlignment = UITextAlignmentLeft;
        glblRemark.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblRemark.backgroundColor = [UIColor clearColor];
        self.lblRemark = glblRemark;
        [glblRemark release];
        [self addSubview:lblRemark];
        
        UILabel *glblDateText = [[UILabel alloc] initWithFrame:CGRectMake(120, 40, 455, 21)];
        glblDateText.textAlignment = UITextAlignmentLeft;
        glblDateText.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
        glblDateText.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblDateText.backgroundColor = [UIColor clearColor];
        self.lblDateText = glblDateText;
        [glblDateText release];
        [self addSubview:lblDateText];
        
        
        UILabel *glblStatustext = [[UILabel alloc] initWithFrame:CGRectMake(120, 60, 455, 21)];
        glblStatustext.textAlignment = UITextAlignmentLeft;
        glblStatustext.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblStatustext.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
        glblStatustext.backgroundColor = [UIColor clearColor];
        self.lblStatustext = glblStatustext;
        [glblStatustext release];
        [self addSubview:lblStatustext]; 
        
        UILabel *glblRemarktext = [[UILabel alloc] initWithFrame:CGRectMake(120, 80, 455, 100)];
        glblRemarktext.textAlignment = UITextAlignmentLeft;
        glblRemarktext.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblRemarktext.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
        glblRemarktext.backgroundColor = [UIColor clearColor];
        self.lblRemarktext = glblRemarktext;
        [glblRemarktext release];
        [self addSubview:lblRemarktext];
        
        UIImage *imgbtnsaveEvent = [UIImage imageNamed:@"icon_add_calendar.png"];
        UIButton *gbtnsaveAppointment = [[UIButton alloc] initWithFrame:CGRectMake(250, 35, 60, 63)];
        [gbtnsaveAppointment setBackgroundImage:imgbtnsaveEvent forState:UIControlStateNormal];
        [gbtnsaveAppointment addTarget:self action:@selector(BtnSaveAppointmentTapped:) forControlEvents:UIControlEventTouchDown];
        self.btnsaveAppointment = gbtnsaveAppointment;
        [gbtnsaveAppointment release];
        [self addSubview:btnsaveAppointment];
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(IBAction)BtnSaveAppointmentTapped:(id)sender
{
    [self.delegate addAppointmentTapped:(BpAppointmentList*)AppointmentListcell];
}


-(void)dealloc
{
    [imgsmallcancel release];
    [btnsaveAppointment release];
    [lblStatustext release];
    [lblStatus release];
    [delegate release];
    [lblRemarktext release];
    [lblDateText release];
    [lblRemark release];
    [AppointmentListcell release];
    [imgsmallflag release];
    [lineColor release];
    [lblBodytext release];
    [lblDate release];
    [lblTitle release];
    [super dealloc];}


@end
