//
//  BpEventTableViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpEventTableViewController.h"

#define kShadeViewTag 1000
@implementation BpEventTableViewController
@synthesize eventArr;
@synthesize gBpEventViewController;
@synthesize enlargeImagePath;
//@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self initializeTableData];
    }
    return self;
}

-(void)dealloc
{
    //[delegate release];
    [enlargeImagePath release];
    [eventArr release];
    [gBpEventViewController release];
    [super dealloc];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    /*if(self.eventArr!=nil)
    {
        return [self.eventArr count];
    }*/
    
    return [self.eventArr count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    cell = (BpEventCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //BpEventCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[BpEventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    BpEvent* gBpEventcell=[self.eventArr objectAtIndex:indexPath.row];
    
    cell.delegate =self.gBpEventViewController;
    cell.BpEventcell=gBpEventcell;
    
    
    cell.imgsmallflag.image=[UIImage imageNamed:@"icon_events_2.png"];;
    
    
    if (gBpEventcell.eventsImgPath.length!=0) 
    {
        
        
        /*=============*/
        
        CGSize maxsizetitle = CGSizeMake(180, 9999);
        UIFont *thefonttitle = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        CGSize textsizetitle = [gBpEventcell.eventsTitle sizeWithFont:thefonttitle constrainedToSize:maxsizetitle lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblTitle.numberOfLines=20;
        cell.lblTitle.frame=CGRectMake(80, 10, 180, textsizetitle.height);
        cell.lblTitle.lineBreakMode=UILineBreakModeWordWrap;
        cell.lblTitle.text=gBpEventcell.eventsTitle;
        
        
        CGSize maxsize = CGSizeMake(220, 9999);
        UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
        CGSize textsize = [gBpEventcell.eventsDescp sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblBodytext.numberOfLines=20;
        cell.lblBodytext.frame=CGRectMake(80, textsizetitle.height+50, 220, textsize.height);
        cell.lblBodytext.lineBreakMode=UILineBreakModeWordWrap;
        cell.lblBodytext.text=gBpEventcell.eventsDescp;
        
        
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsizetitle.height+textsize.height+80);
        
        /*=============*/
        
        //NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/event/%@",gBpEventcell.eventsImgPath];
        //cell.imgPath=imagename;
        
        //cell.imgpicture.imageURL = [NSURL URLWithString:imagename];
        //cell.imgpicture.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagename]]];
        
        
        cell.lblDate.frame=CGRectMake(80, textsizetitle.height+10, 180, 45);
        cell.lblDate.text=@"Date : ";
        
        cell.lblDatetext.frame=CGRectMake(120, textsizetitle.height+10, 180, 45);
        cell.lblDatetext.text=gBpEventcell.eventsDateTime;
        
        //self.enlargeImagePath=imagename;
        
        //[cell.btnenlargeimg setBackgroundImage:imgbtnsaveEvent forState:UIControlStateNormal];
        
        /*UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageTapped)];
        [cell.imgpicture addGestureRecognizer:singletap];
        [cell.imgpicture setUserInteractionEnabled:YES];
        [singletap release];*/
        
        cell.btnsaveEvent.frame=CGRectMake(255, 10, 60, 63);
        
        NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/event/%@",gBpEventcell.eventsImgPath];
        cell.url=imagename;
        cell.imgpicture.imageURL = [NSURL URLWithString:imagename];
        /*cell.imgpicture.frame = CGRectMake(10, textsizetitle.height+28, 65, 65);
        if(cell.imgpicture.image != nil)
        {
            float height = cell.imgpicture.image.size.height * (float)55 / cell.imgpicture.image.size.width;
            //[cell.imgpicture setFrame:CGRectMake(10, 50, 65, height)];
            NSLog(@"height--->%f",height);
            //cell.imgpicture.hidden=NO;
            cell.imgpicture.frame = CGRectMake(10, textsizetitle.height+50, 65, height);
            
        }else 
        {
            //cell.imgpicture.hidden=YES;
            NSLog(@"nilll");
        }*/
        
    }else 
    {
        /*=============*/
        CGSize maxsize = CGSizeMake(220, 9999);
        UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
        CGSize textsize = [gBpEventcell.eventsDescp sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        cell.lblBodytext.numberOfLines=20;
        cell.lblBodytext.frame=CGRectMake(10, 35, 220, textsize.height+50);
        
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsize.height+100);
        
        /*=============*/
        cell.imgpicture.image=nil;
        cell.lblBodyNoPictext.text=gBpEventcell.eventsImgPath;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;

}

-(void)ImageTapped
{
    NSLog(@"hello!!!");
    NSLog(@"self.enlargeImagePath!!!--->%@",self.enlargeImagePath);
    //NSString *largeimage = [imagepath stringByReplacingCharactersInRange:NSMakeRange(imagepath.length - 5, 1) withString:@"M"];
    //[MTPopupWindow showWindowWithHTMLFile:largeimage insideView:self.view];
    [MTPopupWindow showWindowWithHTMLFile:self.enlargeImagePath insideView:self.view];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BpEventCell *selectedcell= (BpEventCell*)[tableView cellForRowAtIndexPath:indexPath]; 
    
    
    NSLog(@"selected row~");
    
    NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/event/%@",selectedcell.BpEventcell.eventsImgPath];
    //UIViewController *viewcontroller = [[UIViewController alloc]init];
    //UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0,100, 100)];
    //viewcontroller.view = view;
    //self.gBpEventViewController = viewcontroller;
    
    
    //UIViewController *viewcontroller = [[UIViewController alloc]init];
    //viewcontroller.parentViewController.view=gBpEventViewController;
    //[MTPopupWindow showWindowWithHTMLFile:imagename insideView:self.view];
    
    //[self.delegate imgurlTapped];
    /*UIView* fauxView = [[[UIView alloc] initWithFrame: CGRectMake(10, 10, 200, 200)] autorelease];
    [bgView addSubview: fauxView];
    
    bigPanelView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, bgView.frame.size.width, bgView.frame.size.height)] autorelease];
    bigPanelView.center = CGPointMake( bgView.frame.size.width/2, bgView.frame.size.height/2);
    
    
    UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagename]]];
    UIImageView* background=nil;
    background = [[[UIImageView alloc] initWithImage:image] autorelease];
    
    
    background.center = CGPointMake(bigPanelView.frame.size.width/2, bigPanelView.frame.size.height/2);
    
    int closeBtnOffset = 5;
    UIImage* closeBtnImg = [UIImage imageNamed:@"popupCloseBtn.png"];
    UIButton* closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setImage:closeBtnImg forState:UIControlStateNormal];
    //[closeBtn setFrame:CGRectMake( background.frame.origin.x + background.frame.size.width - closeBtnImg.size.width - closeBtnOffset, background.frame.origin.y , closeBtnImg.size.width + closeBtnOffset, closeBtnImg.size.height)];
    
    [closeBtn setFrame:CGRectMake( background.frame.origin.x + background.frame.size.width - closeBtnOffset,
                                  background.frame.origin.y - closeBtnImg.size.height + closeBtnOffset,
                                  closeBtnImg.size.width , 
                                  closeBtnImg.size.height )];
    
    [closeBtn addTarget:self action:@selector(closePopupWindow:) forControlEvents:UIControlEventTouchUpInside];
    //[teditButton addTarget:self action:@selector(edit_ButtonTapped:)forControlEvents:UIControlEventTouchDown];
    [bigPanelView addSubview: closeBtn];
    
    //animation options
    UIViewAnimationOptions options = UIViewAnimationOptionTransitionFlipFromRight |
    UIViewAnimationOptionAllowUserInteraction    | 
    UIViewAnimationOptionBeginFromCurrentState;
    
    //run the animation
    [UIView transitionFromView:fauxView toView:bigPanelView duration:0.5 options:options completion: ^(BOOL finished) {
        
        //dim the contents behind the popup window
        UIView* shadeView = [[[UIView alloc] initWithFrame:bigPanelView.frame] autorelease];
        shadeView.backgroundColor = [UIColor blackColor];
        shadeView.alpha = 0.3; 
        shadeView.tag = kShadeViewTag;
        [bigPanelView addSubview: shadeView];
        [bigPanelView sendSubviewToBack: shadeView];
    }];*/
    
    /*
    BpPromotionImageViewController *gBpPromotionImageViewController=[[BpPromotionImageViewController alloc] initWithNibName:@"BpPromotionImageViewController" bundle:nil];
    gBpPromotionImageViewController.title=@"Health Tips";
    gBpPromotionImageViewController.imgPath=imagename;
    [self.navigationController pushViewController:gBpPromotionImageViewController animated:YES];
    [gBpPromotionImageViewController release];
    */
    
}

-(void)initializeTableData
{
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    self.eventArr=appDelegate.EventArr;
}
@end
