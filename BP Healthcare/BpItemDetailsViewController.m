//
//  BpItemDetailsViewController.m
//  BP Healthcare
//
//  Created by desmond on 13-1-8.
//
//

#import "BpItemDetailsViewController.h"

@interface BpItemDetailsViewController ()

@end

@implementation BpItemDetailsViewController
@synthesize promotionItem;
@synthesize quantity;
@synthesize gBpOrderDetailsViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    BpItemDetailsViewController *gBpLatestPromotionViewController=[[BpItemDetailsViewController alloc] initWithNibName:@"BpItemDetailsViewController" bundle:nil];
    
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease];
    gBpLatestPromotionViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    self.navigationItem.title = @"Item Details";
    [leftButton release];
    [gBpLatestPromotionViewController release];
    
    quantity = 1;
    
    _quantityLabel.text = [[NSString alloc] initWithFormat:@"%d",quantity];
    _quantityLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mid_box.png"]];
    _titleLabel.text = promotionItem.Name;
    _promotionPriceLabel.text = promotionItem.DiscountPrice;
    
    _NPLabel.text = [[NSString alloc] initWithFormat:@"RM %@",promotionItem.NormalPrice];
    UIView* slabel = [[UIView alloc] initWithFrame:CGRectMake(_NPLabel.frame.origin.x, _NPLabel.frame.origin.y+5, _NPLabel.frame.size.width, 1)];
    [self.view addSubview:slabel];
    [slabel setBackgroundColor:[UIColor grayColor]];
    
    //set content to _descptionLabel and resize the scroll view
    _descptionLabel.text = promotionItem.Descp;
    _descptionLabel.numberOfLines = 0;
    [_descptionLabel sizeToFit];
    CGSize size = _descptionLabel.frame.size;
    [_contentScrollView setContentSize:size];
    
    //add touch action to image view
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [_imageView addGestureRecognizer:singleTap];
    [_imageView setMultipleTouchEnabled:YES];
    [_imageView setUserInteractionEnabled:YES];
    
    [self performSelector:@selector(getimagefromurl) withObject:nil afterDelay:1.0];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    UIView *tappedView = [gesture.view hitTest:[gesture locationInView:gesture.view] withEvent:nil];
    
    BpPromotionImageViewController *gBpItemViewController = [[BpPromotionImageViewController alloc] initWithNibName:@"BpPromotionImageViewController" bundle:nil];
    NSString *imgPath = [[NSString alloc] initWithFormat:@"http://203.106.245.234:100/images/items/%@",promotionItem.PicURl];
    gBpItemViewController.imgPath = imgPath;
    [self.navigationController pushViewController:gBpItemViewController animated:YES];
    [gBpItemViewController release];

    
    NSLog(@"Touch event on view: %@",[tappedView class]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getimagefromurl
{
    NSString *imgPath = [[NSString alloc] initWithFormat:@"http://203.106.245.234:100/images/items/%@",promotionItem.PicURl];
    UIImage *promotionimg=[[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgPath]]];
    
    [self performSelector:@selector(getimage:) withObject:promotionimg afterDelay:1.0];
    
}

-(void)getimage:(UIImage *)img
{
    //promotionimgView=[[[UIImageView alloc] init] autorelease];
    //promotionimgView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
     _imageView.image=img;
    
    CGSize size = img.size;
    float scale = size.height / size.width;
    NSInteger imgHeight = 140;
    float imgWidth = 140/scale;
    
    [_imageView setFrame:CGRectMake((self.view.frame.size.width/2)-(imgWidth/2), 20, imgWidth,imgHeight)];
    _imageView.layer.masksToBounds = YES;
    _imageView.layer.borderColor = [UIColor grayColor].CGColor;
    _imageView.layer.borderWidth = 1;
    
    if(gBpOrderDetailsViewController != nil){
        gBpOrderDetailsViewController.imageView.image = img;
//        NSLog(@"gBpOrderDetailsViewController isn't nil");
    }
   
    
    //NSLog(@"promotionimg.size.height>>>%f",promotionimgView.image.size.height);
}


- (void)dealloc {
    [_imageView release];
    [_titleLabel release];
    [_promotionPriceLabel release];
    [_NPLabel release];
    [_contentScrollView release];
    [_descptionLabel release];
    [_quantityLabel release];
    [_confirmButton release];
    [_cancelButton release];
    [_minusButton release];
    [_addButton release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setImageView:nil];
    [self setTitleLabel:nil];
    [self setPromotionPriceLabel:nil];
    [self setNPLabel:nil];
    [self setContentScrollView:nil];
    [self setDescptionLabel:nil];
    [self setQuantityLabel:nil];
    [self setConfirmButton:nil];
    [self setCancelButton:nil];
    [self setMinusButton:nil];
    [self setAddButton:nil];
    [super viewDidUnload];
}
- (IBAction)quantityAdd:(UIButton *)sender {
    if(quantity < 999)
        quantity += 1;
    _quantityLabel.text = [[NSString alloc] initWithFormat:@"%d",quantity];
}

- (IBAction)quantityMinus:(UIButton *)sender {
    if(quantity > 1)
        quantity -= 1;
    _quantityLabel.text = [[NSString alloc] initWithFormat:@"%d",quantity];
}

- (IBAction)confirmDeal:(UIButton *)sender {
    
    gBpOrderDetailsViewController = [[BpOrderDetailsViewController alloc] initWithNibName:@"BpOrderDetailsViewController" bundle:nil];
    gBpOrderDetailsViewController.itemsQuantity = quantity;
    gBpOrderDetailsViewController.promotionItem = self.promotionItem;
    gBpOrderDetailsViewController.img = self.imageView.image;
    [self.navigationController pushViewController:gBpOrderDetailsViewController animated:YES];
    [gBpOrderDetailsViewController release];

}

- (IBAction)cancelDeal:(UIButton *)sender {
    [self handleBack:sender];
    
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    // pop the controller
    
//    for (UIViewController *temp in self.navigationController.viewControllers) {
//        if ([temp isKindOfClass:[BpLatestPromotionViewController class]]) {
//            [self.navigationController popToViewController:temp animated:YES];
//        }
//    }

    [self.navigationController popViewControllerAnimated:YES];
}


@end
