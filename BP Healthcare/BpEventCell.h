//
//  BpEventCell.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpEvent.h"
#import "BpEventDelegate.h"
#import "AsyncImageView.h"

@interface BpEventCell : UITableViewCell{
    
}
@property (nonatomic, retain) id<BpEventDelegate> delegate;
@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic,retain) UILabel *lblTitle; 
@property (nonatomic, retain) UILabel *lblBodytext;
@property (nonatomic, retain) UILabel *lblBodyNoPictext;
@property (nonatomic, retain) UIImageView *imgsmallflag;
@property (nonatomic, retain) UIImageView *imgpicture;
@property (nonatomic, retain)  UIButton *btnsaveEvent;
@property (nonatomic, retain)  UIButton *btnEventImage;
//@property (nonatomic, retain)  UIButton *btnenlargeimg;
@property (nonatomic, retain) UILabel *lblDatetext;
@property (nonatomic, retain) UILabel *lblDate;
@property (nonatomic, retain) BpEvent *BpEventcell;

@property (nonatomic, retain) NSString *imgPath;
@property (nonatomic, retain) NSString *url;
@end
