//
//  BpHealthTips.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BpHealthTips : NSObject{
    
}

@property (nonatomic,retain) NSString *resultCount;
@property (nonatomic,retain) NSString *healthtipsID;
@property (nonatomic,retain) NSString *healthtipsTitle;
@property (nonatomic,retain) NSString *healthtipsDescp;
@property (nonatomic,retain) NSString *healthtipsSort;
@property (nonatomic,retain) NSString *healthtipsStatus;
@property (nonatomic,retain) NSString *healthtipsDateTime;
@property (nonatomic,retain) NSString *healthtipsImgPath;



@end


/*<resultCount>string</resultCount>
 <healthtipsID>string</healthtipsID>
 <healthtipsTitle>string</healthtipsTitle>
 <healthtipsDescp>string</healthtipsDescp>
 <healthtipsSort>string</healthtipsSort>
 <healthtipsStatus>string</healthtipsStatus>
 <healthtipsDateTime>string</healthtipsDateTime>
 <healthtipsImgPath>string</healthtipsImgPath>*/