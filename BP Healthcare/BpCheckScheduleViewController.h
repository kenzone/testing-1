//
//  BpCheckScheduleViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BpCheckUp.h"
#import "BpCheckUpRequest.h"
#import "BpCheckUpResponse.h"
#import "NetworkHandler.h"
#import "Reachability.h"
#import "BpCheckUpCell.h"
#import "LoadingView.h"

@interface BpCheckScheduleViewController : UIViewController<NetworkHandlerDelegate,UITableViewDelegate, UITableViewDataSource>{
    BpCheckUpCell *cell;
}
-(void)getCheckUp;

@property (retain, nonatomic) IBOutlet UITableView *tblCheckUp;
@property (retain, nonatomic) NSArray *CheckArr;
@property (nonatomic, retain) LoadingView *loadingView;
@property (retain, nonatomic) IBOutlet UIImageView *Bgimg;
@property (retain, nonatomic) IBOutlet UILabel *lblnoItem;

@end
