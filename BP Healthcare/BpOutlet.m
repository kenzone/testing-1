//
//  BpOutlet.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpOutlet.h"

@implementation BpOutlet
@synthesize resultCount, BranchImg, BranchName, Address_1, Address_2, Address_3, Tel, Fax, LocationID, BusinessHourImg, TypeDC, TypeLAB, TypeHEARING,TypeDISPENSARY, MapURL, Latitude, Longitude, Visibility, LocationImg, RowID;
@synthesize currentLatitude, currentLongitude;
-(void)dealloc
{
    [currentLongitude release];
    [currentLatitude release];
    [resultCount release];
    [BranchImg release];
    [BranchName release];
    [Address_1 release];
    [Address_2 release];
    
    [Address_3 release];
    [Tel release];
    [Fax release];
    [BusinessHourImg release];
    [TypeDC release];
    
    [TypeLAB release];
    [TypeHEARING release];
    [TypeDISPENSARY release];
    [MapURL release];
    [Latitude release];
    
    [Longitude release];
    [Visibility release];
    [LocationImg release];
    [RowID release];
    [LocationID release];
    
    
    [super dealloc];
}

@end

