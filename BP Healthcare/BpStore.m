//
//  BpStore.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpStore.h"

@implementation BpStore

@synthesize storeName,description,phoneNumber,faxNumber,storeAddress;
@synthesize location,storeRating,numberOfRatingVotes,storeMainImage;
@synthesize storeFirstImage,storeSecondImage,storeThirdImage,facilityArray;
-(void)dealloc
{
    [facilityArray release];
	[storeName release];
	[description release];
	[phoneNumber release];
	[faxNumber release];
	[storeAddress release];
	[storeMainImage release];
	[storeFirstImage release];
	[storeSecondImage release];
	[storeThirdImage release];
	[location release];
	[super dealloc];
}

- (CLLocationCoordinate2D) coordinate 
{
	CLLocationCoordinate2D coord = {self.location.latitude, self.location.longitude};
	return coord;
}


- (NSString *)title
{
	return self.storeName;
}

@end
