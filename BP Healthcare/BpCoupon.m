//
//  BpCoupon.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpCoupon.h"

@implementation BpCoupon
@synthesize resultCount,couponID,couponSort,couponDescp, couponTitle, couponStatus, couponImgPath,couponDateTime;

-(void)dealloc
{
    [couponDateTime release];
    [couponDescp release];
    [couponID release];
    [couponImgPath release];
    [couponSort release];
    [couponStatus release];
    [couponTitle release];
    [resultCount release];
    [super dealloc];
}

@end
