//
//  BpCouponRequest.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpCouponRequest.h"

@implementation BpCouponRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"retrieveCouponList";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}

@end
