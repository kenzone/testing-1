//
//  BpChooseStateViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "BpOutlet.h"
#import "BpOutletRequest.h"
#import "BpOutletResponse.h"
#import "NetworkHandler.h"
#import "LoadingView.h"

//#import "BpNewAppointmentViewController.h"

@class BpNewAppointmentViewController;

@interface BpChooseStateViewController : UIViewController<NetworkHandlerDelegate>{
    
}

@property (retain, nonatomic) IBOutlet UITableView *tblSelectState;
@property (retain, nonatomic) NSArray * State;
@property (retain, nonatomic) NSString *SelectedState;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) BpNewAppointmentViewController *gBpNewAppointmentViewController;

@end
