//
//  BpHealthTipsViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "NetworkHandler.h"
#import "Reachability.h"
#import "BpHealthTips.h"
#import "BpHealthTipsRequest.h"
#import "BpHealthTipsResponse.h"
#import "BpHealthyTipsCell.h"
#import "LoadingView.h"
#import "AsyncImageView.h"
#import "MTPopupWindow.h"
#import "BpPromotionImageViewController.h"

@interface BpHealthTipsViewController : UIViewController<NetworkHandlerDelegate,UITableViewDelegate, UITableViewDataSource>{
    BpHealthyTipsCell *cell;
}
-(void)getHealthTips;
@property (nonatomic, retain) NSArray *healthArr;
@property (retain, nonatomic) IBOutlet UITableView *tblhealthtips;
@property (nonatomic, retain) LoadingView *loadingView;
@property (retain, nonatomic) IBOutlet UIImageView *Bgimg;
@property (retain, nonatomic) IBOutlet UILabel *lblnoItem;

@end
