//
//  BpGetAllItem.m
//  BP Healthcare
//
//  Created by desmond on 13-1-7.
//
//

#import "BpGetAllItem.h"

@implementation BpGetAllItem

-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"GetAllItems";
        self.requestType=WebserviceRequest;
    }
    return self;
}

-(NSString*) generateHTTPPostMessage
{
    return @"";
}


@end
