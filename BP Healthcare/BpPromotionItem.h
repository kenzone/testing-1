//
//  BpPromotionItem.h
//  BP Healthcare
//
//  Created by desmond on 13-1-7.
//
//

#import <Foundation/Foundation.h>

@interface BpPromotionItem : NSObject

@property (copy, nonatomic) NSString *Name;
@property (copy, nonatomic) NSString *CreateDate;
@property (copy, nonatomic) NSString *EditDate;
@property (copy, nonatomic) NSString *Descp;
@property (copy, nonatomic) NSString *DiscountPrice;
@property (copy, nonatomic) NSString *NormalPrice;
@property (copy, nonatomic) NSString *PicURl;

@end
