//
//  BpFestival.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpFestival.h"

@implementation BpFestival
@synthesize resultCount,festivalID,festivalTitle,festivalSort,festivalDescp,festivalStatus,festivalImgPath,festivalCreateDate;

-(void)dealloc
{
    [festivalCreateDate release];
    [festivalDescp release];
    [festivalID release];
    [festivalImgPath release];
    [festivalSort release];
    [festivalTitle release];
    [festivalStatus release];
    [resultCount release];
    [super dealloc];
}
@end
