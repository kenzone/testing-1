//
//  BpAppPushtblViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppPushtblViewController.h"

@interface BpAppPushtblViewController ()

@end

@implementation BpAppPushtblViewController
@synthesize gBpAppointmentViewController, AppointmentLsArr;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self initializeTableData];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)dealloc
{
    [gBpAppointmentViewController release];
    [AppointmentLsArr release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.AppointmentLsArr count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    cell = (BpAppPushCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    
    
    //static NSString *CellIdentifier = @"Cell";
    //BpEventCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[BpAppPushCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    BpAppointmentList* gBpEventcell=[self.AppointmentLsArr objectAtIndex:indexPath.row];
    
    cell.delegate =self.gBpAppointmentViewController;
    cell.AppointmentListcell=gBpEventcell;
    
    cell.imgsmallflag.image=[UIImage imageNamed:@"icon_appointment.png"];
    cell.imgsmallcancel.image=[UIImage imageNamed:@"btn_delete.png"];
    
    if ([gBpEventcell.appointmentBookDateTime isEqualToString:@"(null)"]) {
        cell.lblDateText.text=@"";
    }else {
        cell.lblDateText.text=gBpEventcell.appointmentBookDateTime;
    }
    
    if ([gBpEventcell.appointmentStatus isEqualToString:@"(null)"]) {
        cell.lblStatustext.text=@"";
    }else {
        cell.lblStatustext.text=gBpEventcell.appointmentStatus;
    }
    
    
    
    
    /*=============*/
    
    CGSize maxsizetitle = CGSizeMake(220, 9999);
    UIFont *thefonttitle = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    CGSize textsizetitle = [gBpEventcell.appointmentServiceRequest sizeWithFont:thefonttitle constrainedToSize:maxsizetitle lineBreakMode:UILineBreakModeWordWrap];
    
    cell.lblTitle.numberOfLines=20;
    cell.lblTitle.frame=CGRectMake(70, 15, 220, textsizetitle.height);
    cell.lblTitle.lineBreakMode=UILineBreakModeWordWrap;
    cell.lblTitle.text=gBpEventcell.appointmentServiceRequest;
    
    CGSize maxsize = CGSizeMake(160, 9999);
    UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
    CGSize textsize = [gBpEventcell.adminRemark sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    
    
    cell.lblRemarktext.numberOfLines=20;
    cell.lblRemarktext.frame=CGRectMake(70, textsizetitle.height+68, 160, textsize.height);
    cell.lblRemarktext.lineBreakMode=UILineBreakModeWordWrap;
    
    if ([gBpEventcell.adminRemark isEqualToString:@"(null)"]) {
        cell.lblRemarktext.text=@"";
    }else {
        cell.lblRemarktext.text=gBpEventcell.adminRemark;
    }
    
    
    cell.lblRemark.frame=CGRectMake(10, textsizetitle.height+65, 120, 21);
    
    cell.lblDate.frame=CGRectMake(10, textsizetitle.height+27, 100, 21);
    cell.lblDateText.frame=CGRectMake(70, textsizetitle.height+27, 455, 21);
    
    cell.lblStatus.frame=CGRectMake(10, textsizetitle.height+45, 100, 21);
    cell.lblStatustext.frame=CGRectMake(70, textsizetitle.height+45, 455, 21);
    
    cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsizetitle.height+textsize.height+100);
    
    
    /*=============*/
    
    
    
    if (![gBpEventcell.appointmentStatus isEqualToString:@"Confirmed"]) {
        cell.btnsaveAppointment.hidden=YES;
        cell.imgsmallcancel.frame=CGRectMake(270, textsizetitle.height+45, 30, 30);
        cell.imgsmallcancel.hidden=YES;
    }else {
        cell.btnsaveAppointment.hidden=NO;
        cell.btnsaveAppointment.frame=CGRectMake(250, textsizetitle.height+30, 60, 63);
        //cell.imgsmallcancel.frame=CGRectMake(270, textsizetitle.height+45, 30, 30);
        cell.imgsmallcancel.hidden=YES;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

-(void)initializeTableData
{
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    self.AppointmentLsArr=appDelegate.AppointmentArr;
}

@end
