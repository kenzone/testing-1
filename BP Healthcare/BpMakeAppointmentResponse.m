//
//  BpMakeAppointmentResponse.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpMakeAppointmentResponse.h"

@implementation BpMakeAppointmentResponse
@synthesize AppointmentResponseArr;

-(void)dealloc
{
    [AppointmentResponseArr release];
    [super dealloc];
}


@end
