//
//  BpFestivalViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkHandler.h"
#import "BpFestival.h"
#import "BpFestivalRequest.h"
#import "BpFestivalResponse.h"
#import "Reachability.h"

@interface BpFestivalViewController : UIViewController<NetworkHandlerDelegate>
{
    
}
-(void)getFestival;

@end
