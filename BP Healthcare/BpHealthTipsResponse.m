//
//  BpHealthTipsResponse.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpHealthTipsResponse.h"

@implementation BpHealthTipsResponse
@synthesize HealthTipsArr;

-(void)dealloc
{
    [HealthTipsArr release];
    [super dealloc];
}

@end
