//
//  BpBirthdayViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkHandler.h"
#import "BpBirthday.h"
#import "BpBirthdayRequest.h"
#import "BpBirthdayResponse.h"
#import "Reachability.h"
#import "LoadingView.h"

@interface BpBirthdayViewController : UIViewController<NetworkHandlerDelegate>{
    
}

-(void)getBirthday;
@property (nonatomic, retain) UIImageView *imgpicture;
@property (retain, nonatomic) IBOutlet UIImageView *imgPicbirthday;
@property (nonatomic, retain) LoadingView *loadingView;

@end
