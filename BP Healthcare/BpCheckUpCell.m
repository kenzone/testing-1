//
//  BpCheckUpCell.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpCheckUpCell.h"

@implementation BpCheckUpCell
@synthesize lblDate, lblTitle, lblBodytext, lineColor, imgsmallflag, BpCheckUpcell;
@synthesize lblRemark, lblDateText, lblRemarktext;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        UIImageView *gimgsmallflag= [[UIImageView alloc]initWithFrame:CGRectMake(30, 10, 20, 21)];        
        //gimgsmallflag.image=[UIImage imageNamed:@"btn_more.png"];
        self.imgsmallflag=gimgsmallflag;
        [gimgsmallflag release];
        [self addSubview:imgsmallflag];
        
        UILabel *glblTitle = [[UILabel alloc] initWithFrame:CGRectMake(45, 10, 455, 21)];
        glblTitle.textAlignment = UITextAlignmentLeft;
        glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        glblTitle.backgroundColor = [UIColor clearColor];
        self.lblTitle = glblTitle;
        [glblTitle release];
        [self addSubview:lblTitle];
        
        /*UILabel *glblDate = [[UILabel alloc] initWithFrame:CGRectMake(45, 40, 50, 21)];
        glblDate.text=@"Date : ";
        glblDate.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
        glblDate.textAlignment = UITextAlignmentLeft;
        glblDate.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblDate.backgroundColor = [UIColor clearColor];
        self.lblDate = glblDate;
        [glblDate release];
        [self addSubview:lblDate];*/
        
        UILabel *glblRemark = [[UILabel alloc] initWithFrame:CGRectMake(45, 40, 100, 21)];
        glblRemark.text=@"Remark  ";
        glblRemark.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblRemark.textAlignment = UITextAlignmentLeft;
        glblRemark.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblRemark.backgroundColor = [UIColor clearColor];
        self.lblRemark = glblRemark;
        [glblRemark release];
        [self addSubview:lblRemark];
        
        /*UILabel *glblDateText = [[UILabel alloc] initWithFrame:CGRectMake(110, 40, 455, 21)];
        glblDateText.textAlignment = UITextAlignmentLeft;
        glblDateText.textColor = [UIColor blackColor];
        glblDateText.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
        glblDateText.backgroundColor = [UIColor clearColor];
        self.lblDateText = glblDateText;
        [glblDateText release];
        [self addSubview:lblDateText];*/
        
        UILabel *glblRemarktext = [[UILabel alloc] initWithFrame:CGRectMake(110, 40, 220, 50)];
        glblRemarktext.textAlignment = UITextAlignmentLeft;
        //glblRemarktext.numberOfLines=2;
        glblRemarktext.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblRemarktext.textColor = [UIColor colorWithRed:(60/255.0) green:(60/255.0) blue:(60/255.0) alpha:1.0];
        glblRemarktext.backgroundColor = [UIColor clearColor];
        self.lblRemarktext = glblRemarktext;
        [glblRemarktext release];
        [self addSubview:lblRemarktext];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)dealloc
{
    [lblRemarktext release];
    [lblDateText release];
    [lblRemark release];
    [BpCheckUpcell release];
    [imgsmallflag release];
    [lineColor release];
    [lblBodytext release];
    [lblDate release];
    [lblTitle release];
    [super dealloc];
}
@end
