//
//  BpPromotionItemCell.h
//  BP Healthcare
//
//  Created by desmond on 13-1-8.
//
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface BpPromotionItemCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *promotionPriceLabel;
@property (retain, nonatomic) IBOutlet UILabel *NPLabel;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;

@property (retain, nonatomic) NSString *url;

-(void)setData:(NSString *)data UILabel:(UILabel *)label;

-(void)setImageSrc:(NSString *)data UILabel:(UIImageView *)image;

@end
