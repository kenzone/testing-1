//
//  BpHealthTipsResponse.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"

@interface BpHealthTipsResponse : XMLResponse{
    
}

@property (nonatomic,retain) NSArray *HealthTipsArr;

@end
