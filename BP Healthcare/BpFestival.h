//
//  BpFestival.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BpFestival : NSObject{
    
}

@property (nonatomic,retain) NSString *resultCount;
@property (nonatomic,retain) NSString *festivalID;
@property (nonatomic,retain) NSString *festivalTitle;
@property (nonatomic,retain) NSString *festivalDescp;
@property (nonatomic,retain) NSString *festivalSort;
@property (nonatomic,retain) NSString *festivalStatus;
@property (nonatomic,retain) NSString *festivalCreateDate;
@property (nonatomic,retain) NSString *festivalImgPath;


@end

