//
//  BpPromotions.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BpPromotions : NSObject
{
    
}

@property (nonatomic,retain) NSString *resultCount;
@property (nonatomic,retain) NSString *promotionID;
@property (nonatomic,retain) NSString *promotionTitle;
@property (nonatomic,retain) NSString *promotionDescp;
@property (nonatomic,retain) NSString *promotionSort;
@property (nonatomic,retain) NSString *promotionStatus;
@property (nonatomic,retain) NSString *promotionDateTime;
@property (nonatomic,retain) NSString *promotionImgPath;

@end


