//
//  BpUpdateOrderRequest.h
//  BP Healthcare
//
//  Created by desmond on 13-1-23.
//
//

#import "XMLRequest.h"

@interface BpUpdateOrderRequest : XMLRequest

@property (retain, nonatomic) NSString *referenceNo;
@property (retain, nonatomic) NSString *remark;
@property (retain, nonatomic) NSString *transactionid;
@property (retain, nonatomic) NSString *authcode;
@property (retain, nonatomic) NSString *amount;
@property (retain, nonatomic) NSString *paymentstatus;

@end
