//
//  BpAppPushViewController.m
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BpAppPushViewController.h"

@interface BpAppPushViewController ()

@end

@implementation BpAppPushViewController
@synthesize AppointmentLsArr, loadingView;
@synthesize tblAppointmentList;
@synthesize gBpAppointmentLsViewController;
@synthesize defaultCalendar, detailViewController, eventsList, eventStore;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpAppPushViewController *gBpAppointmentViewController=[[BpAppPushViewController alloc] initWithNibName:@"BpAppPushViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];    
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpAppointmentViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpAppointmentViewController release];
    
    [self getAppointmentList];
    
    
    UIButton* buttonLogoutTapped = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLogoutTapped.tag = 1;
    buttonLogoutTapped.frame = CGRectMake(0, 0, 62, 33);
    //buttonLogoutTapped.titleLabel.text=@"Home";
    [buttonLogoutTapped setBackgroundImage:[UIImage imageNamed:@"btn_add.png"]  forState:UIControlStateNormal];
    [buttonLogoutTapped addTarget:self action:@selector(NewAppointmentTapped) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:buttonLogoutTapped];  
    //self.navigationItem.leftBarButtonItem.title = @"Log Out";
    self.navigationItem.rightBarButtonItem = btnLogOut;
    [btnLogOut release];
    
    /*===========Appointment table================*/
    if(gBpAppointmentLsViewController == nil)
    {
        BpAppPushtblViewController *gBpAppointmentLsViewController = [[BpAppPushtblViewController alloc] init];
        self.gBpAppointmentLsViewController = gBpAppointmentLsViewController;
        self.gBpAppointmentLsViewController.gBpAppointmentViewController=self;
        [gBpAppointmentLsViewController release];
    } 
    
    [tblAppointmentList setDataSource:self.gBpAppointmentLsViewController];
    [tblAppointmentList setDelegate:self.gBpAppointmentLsViewController];
    
    
    self.gBpAppointmentLsViewController.view = self.gBpAppointmentLsViewController.tableView;
    //self.tblAppointmentList.rowHeight = 130;
    
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
}

-(void)NewAppointmentTapped
{
    BpNewAppointmentViewController *gBpNewAppointmentViewController=[[BpNewAppointmentViewController alloc] initWithNibName:@"BpNewAppointmentViewController" bundle:nil];
    
    gBpNewAppointmentViewController.title=@"New Appointment";
    [self.navigationController pushViewController:gBpNewAppointmentViewController animated:YES];
    [gBpNewAppointmentViewController release];
}

- (void)viewDidUnload
{
    [self setTblAppointmentList:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getAppointmentList
{
    BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSLog(@"appDelegate.TokenString--->%@",appDelegate.TokenString);
    
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    BpAppointmentLsRequest *loginset= [[BpAppointmentLsRequest alloc] initWithsTokenId:appDelegate.TokenString];
    
    [networkHandler setDelegate:self];
    [networkHandler request:loginset];
    [loginset release];
    [networkHandler release];
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[BpAppointmentLsResponse class]])
    {
        
        //NSString *strMessage=[NSString stringWithFormat:@"%@",((BpOutletResponse*)responseMessage).OutletArr];
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((BpAppointmentLsResponse*)responseMessage).AppointmentLsArr;
        self.AppointmentLsArr=msgArr;
        //self.storesArray=msgArr;
        
        //NSLog(@"strMessage--->%@",strMessage);
        
        
        /*for (BpAppointmentList *item in msgArr) {
         //birthitem.resultCount;
         
         NSLog(@"appointmentID--->%@",item.appointmentID);
         NSLog(@"userID--->%@",item.userID);
         
         
         }*/
        
        BpAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        appDelegate.AppointmentArr=msgArr;
        
        [self.gBpAppointmentLsViewController initializeTableData];
        [tblAppointmentList reloadData];
        [self.loadingView removeView];
        
    }else {
        /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Server down. Please try again later. Thanks you." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        [self.loadingView removeView];
    } 
    
}

#pragma mark -

#pragma mark Add a new Event
-(IBAction) addEvent:(id)sender {
    
    self.eventStore = [[EKEventStore alloc] init];
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    //  event.title =@"Whatever you want your title to be";
    event.title = @"";// self.currentTitle;
    
    /**/NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
    NSDate *myDate = [df dateFromString: @"16/08/2012 02:00  pm"];
    //startDate=@"";
    
    event.startDate=myDate;
    event.endDate=[df dateFromString: @"16/08/2012 02:00  pm"];
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    event.allDay = YES;
    
    EKEventEditViewController *addController = [[EKEventEditViewController alloc] initWithNibName:nil bundle:nil];
    addController.event = event;
    addController.eventStore = self.eventStore;
    [self presentModalViewController:addController animated:YES];
    
    addController.editViewDelegate = self;
    [addController release];
    
}

-(void)addAppointmentTapped:(BpAppointmentList*)iBpAppointmentList
{
    self.eventStore = [[EKEventStore alloc] init];
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    //  event.title =@"Whatever you want your title to be";
    event.title = iBpAppointmentList.appointmentServiceRequest;// self.currentTitle;
    event.notes = iBpAppointmentList.adminRemark;
    /**/NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy hh:mm a"];
    NSDate *myDate = [df dateFromString: iBpAppointmentList.appointmentBookDateTime];
    //startDate=@"";
    
    event.startDate = [[NSDate alloc] initWithTimeInterval:0 sinceDate:myDate]; //  works
    event.endDate = [[NSDate alloc] initWithTimeInterval:3600*1 sinceDate:event.startDate]; // works
    
    event.allDay = YES;
    
    EKEventEditViewController *addController = [[EKEventEditViewController alloc] initWithNibName:nil bundle:nil];
    addController.event = event;
    addController.eventStore = self.eventStore;
    [self presentModalViewController:addController animated:YES];
    
    addController.editViewDelegate = self;
    [addController release];
}


#pragma mark -
#pragma mark EKEventEditViewDelegate

// Overriding EKEventEditViewDelegate method to update event store according to user actions.
- (void)eventEditViewController:(EKEventEditViewController *)controller 
          didCompleteWithAction:(EKEventEditViewAction)action {
    
    NSError *error = nil;
    EKEvent *thisEvent = controller.event;
    
    switch (action) {
        case EKEventEditViewActionCanceled:
            // Edit action canceled, do nothing. 
            break;
            
        case EKEventEditViewActionSaved:
            // When user hit "Done" button, save the newly created event to the event store, 
            // and reload table view.
            // If the new event is being added to the default calendar, then update its 
            // eventsList.
            if (self.defaultCalendar ==  thisEvent.calendar) {
                [self.eventsList addObject:thisEvent];
            }
            [controller.eventStore saveEvent:controller.event span:EKSpanThisEvent error:&error];
            //  [self.tableView reloadData];
            break;
            
        case EKEventEditViewActionDeleted:
            // When deleting an event, remove the event from the event store, 
            // and reload table view.
            // If deleting an event from the currenly default calendar, then update its 
            // eventsList.
            if (self.defaultCalendar ==  thisEvent.calendar) {
                [self.eventsList removeObject:thisEvent];
            }
            [controller.eventStore removeEvent:thisEvent span:EKSpanThisEvent error:&error];
            //[self.tableView reloadData];
            break;
            
        default:
            break;
    }
    // Dismiss the modal view controller
    [controller dismissModalViewControllerAnimated:YES];
    
}


// Set the calendar edited by EKEventEditViewController to our chosen calendar - the default calendar.
- (EKCalendar *)eventEditViewControllerDefaultCalendarForNewEvents:(EKEventEditViewController *)controller {
    EKCalendar *calendarForEdit = self.defaultCalendar;
    return calendarForEdit;
}

-(IBAction)MainViewTappedd:(id)sender
{
    [self mainView];
}

-(void)mainView
{
    /**/for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    
    BpMainViewController *gtBpMainViewController=[[BpMainViewController alloc] initWithNibName:@"BpMainViewController" bundle:nil];
    
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gtBpMainViewController];
    
    tnavController.view.frame=CGRectMake(0, 0, 320, 460);
    gtBpMainViewController.title=@"Main";
    UIImage *imagetopbar = [UIImage imageNamed:@"bar_top.png"];
    [tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    [self.view addSubview:tnavController.view];
}

- (void)dealloc 
{
    [loadingView release];
    [defaultCalendar release];
    [detailViewController release];
    [eventStore release];
    [eventsList release];
    [AppointmentLsArr release];
    [gBpAppointmentLsViewController release];
    [tblAppointmentList release];
    [super dealloc];
}

@end