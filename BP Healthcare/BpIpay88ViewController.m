//
//  BpIpay88ViewController.m
//  BP Healthcare
//
//  Created by desmond on 13-1-15.
//
//

#import "BpIpay88ViewController.h"
#define personalDetailFileName @"personalDetail.plist"

@interface BpIpay88ViewController ()
@property(retain, nonatomic) NSMutableDictionary *paymentResult;
@end

@implementation BpIpay88ViewController
@synthesize paymentsdk, resultDelegate;
@synthesize resultImage, resultView, paymentView,paymentResult;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    BpIpay88ViewController *gBpLatestPromotionViewController=[[BpIpay88ViewController alloc] initWithNibName:@"BpIpay88ViewController" bundle:nil];
    
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease];
    gBpLatestPromotionViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];

    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"bar_top.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    
    self.navigationItem.title = @"Payment";
    [leftButton release];
    [gBpLatestPromotionViewController release];
    
    //get user info
    NSString *filePath = [self dataFilePath];
    if([[NSFileManager defaultManager]  fileExistsAtPath:filePath]){
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
        _userName = [dict objectForKey:@"name"];
        _userEmail = [dict objectForKey:@"email"];
        _userContact = [dict objectForKey:@"contact"];
    }
    
    paymentsdk = [[Ipay alloc] init];
    paymentsdk.delegate = self;
    IpayPayment *payment = [[IpayPayment alloc] init];
    [payment setPaymentId:@"2"];
    [payment setMerchantKey:@"JXqVd2QZu7"];
    [payment setMerchantCode:@"M04204"];
    [payment setRefNo:_referenceNumber];
//    [payment setAmount:_amount];
    [payment setAmount:@"1.00"];
    [payment setCurrency:@"MYR"];
//    [payment setProdDesc:_prodDesc];
//    [payment setUserName:_userName];
//    [payment setUserEmail:_userEmail];
//    [payment setUserContact:_userContact];
    [payment setProdDesc:@"Test Prod"];
    [payment setUserName:@"User"];
    [payment setUserEmail:@"desmond890321@gmail.com"];
    [payment setUserContact:@"01234567"];
    [payment setRemark:@"Testing SDK"];
    [payment setLang:@"ISO-8859-1"];
     paymentView = [paymentsdk checkout:payment];
    [self.view addSubview:paymentView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)dataFilePath{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [path objectAtIndex:0];
    
    return [documentsDirectory stringByAppendingPathComponent:personalDetailFileName];
}

- (void)dealloc {
    [resultView release];
    [resultImage release];
    [super dealloc];
    [_amount dealloc];
    [_userName dealloc];
    [_userEmail dealloc];
    [_userContact dealloc];
    [_remark dealloc];
    [_prodDesc dealloc];
}
- (void)viewDidUnload {
    [self setResultView:nil];
    [self setResultImage:nil];
    [super viewDidUnload];
    [_amount release];
    [_userName release];
    [_userEmail release];
    [_userContact release];
    [_remark release];
    [_prodDesc release];
}

- (void) handleBack:(id)sender
{
    [self paymentCancelled:_referenceNumber withTransId:@"" withAmount:_amount withRemark:@"Customer Cancel Payment" withErrDesc:@"Customer Cancel Payment" ];
}


#pragma Ipay88 delegate method
- (void)paymentSuccess:(NSString *)refNo withTransId:(NSString *)transId withAmount:(NSString *)amount withRemark:(NSString *)remark withAuthCode:(NSString *)authCode{
    NSLog(@"paymentSuccess:  refNo:%@ transId:%@ amount:%@ remark:%@ authCode:%@",
          refNo,transId,amount,remark,authCode);
    [self dismissModalViewControllerAnimated:YES];
    [paymentView removeFromSuperview];
    resultImage.image = [UIImage imageNamed:@"success.png"];
    [self.view addSubview:resultView];
    
    paymentResult = [[NSDictionary alloc] initWithObjectsAndKeys:
                                   refNo,@"referenceNo",
                                   @"Successful",@"paymentStatus",
                                   transId,@"transactionid",
                                   amount,@"amount",
                                   remark,@"remark",
                                   authCode,@"authcode",nil];
    [resultDelegate paymentResult:paymentResult];    
}

- (void)paymentFailed:(NSString *)refNo withTransId:(NSString *)transId withAmount:(NSString *)amount withRemark:(NSString *)remark withErrDesc:(NSString *)errDesc{
    
    [paymentView removeFromSuperview];
    resultImage.image = [UIImage imageNamed:@"fail.png"];
    [self.view addSubview:resultView];
    
    paymentResult = [[NSDictionary alloc] initWithObjectsAndKeys:
                     refNo,@"referenceNo",
                     @"Failed",@"paymentStatus",
                     transId,@"transactionid",
                     amount,@"amount",
                     errDesc,@"remark",
                     @"",@"authcode",nil];
    [resultDelegate paymentResult:paymentResult];

}

- (void)paymentCancelled:(NSString *)refNo withTransId:(NSString *)transId withAmount:(NSString *)amount withRemark:(NSString *)remark withErrDesc:(NSString *)errDesc{
    
    paymentResult = [[NSDictionary alloc] initWithObjectsAndKeys:
                     refNo,@"referenceNo",
                     @"Not yet proccess",@"paymentStatus",
                     transId,@"transactionid",
                     amount,@"amount",
                     errDesc,@"remark",
                     @"",@"authcode",nil];
    [resultDelegate paymentResult:paymentResult];
        [self dismissModalViewControllerAnimated:YES];
}

@end
