//
//  BpContactViewController.h
//  BP Healthcare
//
//  Created by Tiseno Mac 2 on 8/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@class BpNewAppointmentViewController;

@interface BpContactViewController : UIViewController{
    int heightOfEditedView;
    int heightOfEditedArea;
    int heightOffset;
}

@property (retain, nonatomic) IBOutlet UITextView *txtContactno;

@property (nonatomic, retain) BpNewAppointmentViewController *gBpNewAppointmentViewController;
-(IBAction)DoneContacttapped:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btncontactdone;

@end
